from django.shortcuts import render,redirect
import requests

# Create your views here.
def homepage(request):
    return render(request, 'mainapp/homepage.html')
def Leave(request):
    return render(request, 'mainapp/Leave.html')
def Heirarchy(request):
    return render(request, 'mainapp/Heirarchy.html')
def Payroll(request):
    return render(request, 'mainapp/Payroll.html')
def Documentation(request):
    return render(request, 'mainapp/Documentation.html')
def Onboard(request):
    return render(request, 'mainapp/Onboard.html')
def Track(request):
    return render(request, 'mainapp/Track.html')
def create(request):
    if request.POST:
        login_data = request.POST.dict()
        user = login_data.get("username")
        password = login_data.get("password")
        name = login_data.get("fullname")
        email = login_data.get("email")
        
        url="http://mantis.atg.party/api/rest/users/"
        header={
        'Authorization':'mhVBa0ZRB7CCOdd2AGF2RuULv8LCKSp8',
        'Content-Type':'application/json'
        }
        payload = "{\n  \"username\": \"test_user_21\",\n  \"password\": \"p@ssw0rd\",\n  \"real_name\": \"Victor Boctor\",\n  \"email\": \"jecteen@gmail.com\",\n  \"enabled\": true,\n  \"protected\": false\n}" 
        x=requests.post(url,headers=header,data=payload,timeout=100000)
        print(x.status_code)
        print(payload)
        return render(request,'mainapp/Onboard.html')
def delete(request):
    url = 'http://mantis.atg.party/api/rest/users/240'
    headers = {
      'Authorization': 'mhVBa0ZRB7CCOdd2AGF2RuULv8LCKSp8'
    }
    response = requests.request('DELETE', url, headers = headers,timeout=20000)
    print(response.status_code)
    print(response.json())
    return render(request,'mainapp/Onboard.html')