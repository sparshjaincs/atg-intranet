from django.urls import path
from . import views

urlpatterns = [
    path('', views.homepage, name="homepage"),
    path('Leave/',views.Leave,name='Leave'),
    path('Heirarchy/',views.Heirarchy,name='Heirarchy'),
    path('Payroll/',views.Payroll,name='Payroll'),
    path('Documentation/',views.Documentation,name='Documentation'),
    path('Onboard/',views.Onboard,name='Onboard'),
    path('Track/',views.Track,name='Track'),
    path('Onboard/Create/',views.create,name='Create'),
    path('Onboard/delete/',views.delete,name='delete')


    
]
