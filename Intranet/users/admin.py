from django.contrib import admin
from .models import Profile, Department, Position, Emp_position, Project, Works_on

# Register your models here.
admin.site.register(Profile)
admin.site.register(Department)
admin.site.register(Position)
admin.site.register(Emp_position)
admin.site.register(Project)
admin.site.register(Works_on)
