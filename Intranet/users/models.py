from django.db import models
from django.contrib.auth.models import User

from django.db.models.signals import post_save
from django.dispatch import receiver
# Create your models here.

class Profile(models.Model):
    STATUS_CHOICES = (
    (1, ("Permanent")),
    (2, ("Temporary")),
    )
    GENDER_CHOICES = (
    (1, ("Male")),
    (2, ("Female")),
    (3, ("Not Specified"))
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    emp_type = models.IntegerField(choices=STATUS_CHOICES, default=1)
    contact = models.CharField(max_length=13, blank=True)
    whatsapp = models.CharField(max_length=13, blank=True)
    gender = models.IntegerField(choices=GENDER_CHOICES, default=3)
    avatar = models.ImageField(upload_to='users/images', default='users/images/default.jpg')
    manager_username = models.ForeignKey(User, blank=True, null=True, to_field='username',related_name='manager_username', on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class Department(models.Model):
    # since one department can have multiple managers so unique=False
    name = models.CharField(max_length=20, unique=False)
    manager = models.ForeignKey(User, related_name='manager', blank=True, null=True, to_field='username', on_delete=models.DO_NOTHING)
    tech_lead = models.ForeignKey(User, blank=True, null=True, related_name='tech_lead', to_field='username', on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.name

class Position(models.Model):
    # POS_CHOICES = (
    # (1, ("CEO")),
    # (2, ("Dev Manager")),
    # (3, ("Testing Manager")),
    # (4, ("Developer")),
    # (5, ("Tester")),
    # )
    position_name = models.CharField(max_length=20, unique=True)
    def __str__(self):
        return self.position_name


class Emp_position(models.Model):

    emp_uname = models.ForeignKey(User, related_name='emp_name', to_field='username', on_delete=models.CASCADE)
    position_name = models.ForeignKey(Position, related_name='position', to_field='position_name', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.emp_uname) + " " + str(self.position_name)



class Project(models.Model):
    project_name = models.CharField(max_length=20, unique=True)
    project_manager = models.ForeignKey(User, related_name='Project_project_manager', to_field='username', on_delete=models.DO_NOTHING)
    objective = models.TextField()
    parent_project = models.ForeignKey('self', blank=True, null=True, to_field='project_name',related_name='Project_parent_project', on_delete=models.CASCADE)
    obsolete = models.BooleanField()

    def __str__(self):
        return self.project_name



class Works_on(models.Model):
    emp_name = models.ForeignKey(User, related_name='works_emp_name', to_field='username', on_delete=models.CASCADE)
    project_name = models.ForeignKey(Project, blank=True, null=True, to_field='project_name',related_name='works_on_project', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.emp_name) + " " + str(self.project_name)
